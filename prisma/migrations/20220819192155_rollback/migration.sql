/*
  Warnings:

  - You are about to alter the column `cardReader` on the `Entry` table. The data in that column could be lost. The data in that column will be cast from `Int` to `BigInt`.
  - You are about to alter the column `cash` on the `Entry` table. The data in that column could be lost. The data in that column will be cast from `Int` to `BigInt`.
  - You are about to alter the column `incomePayment` on the `Entry` table. The data in that column could be lost. The data in that column will be cast from `Int` to `BigInt`.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Entry" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "cardReader" BIGINT,
    "cash" BIGINT,
    "incomePayment" BIGINT,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL
);
INSERT INTO "new_Entry" ("cardReader", "cash", "createdAt", "id", "incomePayment", "updatedAt") SELECT "cardReader", "cash", "createdAt", "id", "incomePayment", "updatedAt" FROM "Entry";
DROP TABLE "Entry";
ALTER TABLE "new_Entry" RENAME TO "Entry";
CREATE UNIQUE INDEX "Entry_createdAt_key" ON "Entry"("createdAt");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
