-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Cost" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "value" BIGINT NOT NULL,
    "typeId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    CONSTRAINT "Cost_typeId_fkey" FOREIGN KEY ("typeId") REFERENCES "Type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION
);
INSERT INTO "new_Cost" ("createdAt", "id", "typeId", "updatedAt", "value") SELECT "createdAt", "id", "typeId", "updatedAt", "value" FROM "Cost";
DROP TABLE "Cost";
ALTER TABLE "new_Cost" RENAME TO "Cost";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
