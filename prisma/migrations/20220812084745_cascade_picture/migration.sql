-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Picture" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "pic" BLOB NOT NULL,
    "employeeId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    CONSTRAINT "Picture_employeeId_fkey" FOREIGN KEY ("employeeId") REFERENCES "Employee" ("id") ON DELETE CASCADE ON UPDATE SET NULL
);
INSERT INTO "new_Picture" ("createdAt", "employeeId", "id", "pic", "updatedAt") SELECT "createdAt", "employeeId", "id", "pic", "updatedAt" FROM "Picture";
DROP TABLE "Picture";
ALTER TABLE "new_Picture" RENAME TO "Picture";
CREATE UNIQUE INDEX "Picture_employeeId_key" ON "Picture"("employeeId");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
