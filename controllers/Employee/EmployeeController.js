import { PrismaClient } from '@prisma/client';
import * as ImageController from './EmployeeImageController.js';
import * as EmailController from './EmployeeEmailController.js';
import * as PhoneNumberController from './EmployeePhoneNumberController.js';
import * as AddressController from './EmployeeAddressController.js';

const prisma = new PrismaClient();

export const Image = ImageController;
export const Email = EmailController;
export const PhoneNumber = PhoneNumberController;
export const Address = AddressController;

export const create = async (req, res) => {
  try {
    const { firstName, lastName } = req.body;
    const employee = await prisma.employee.create({
      data: {
        firstName,
        lastName
      }
    });
    res.json(employee);
  } catch (error) {
    res.status(500).json({ error });

  }
}
export const get = async (req, res) => {
  try {
    const { id } = req.params;
    const options = {
      where: {
        id: Number(id)
      }
    }

    if (req.query?.include) {
      const include = JSON.parse(req.query.include);
      if (include) {
        options.include = include;
      }
    }

    const profile = await prisma.employee.findUnique(options);

    res.json(profile);
  } catch (error) {
    res.status(500).json({ error });
  }
}

export const getAll = async (req, res) => {
  try {
    const employees = await prisma.employee.findMany();
    res.json(employees);
  } catch (error) {
    res.status(500).json({ error });

  }
}
export const update = async (req, res) => {
  try {
    const { id } = req.params;
    const { firstName, lastName, startWorkAt, endWorkAt } = req.body;
    const employee = await prisma.employee.update({
      where: {
        id: Number(id)
      },
      data: {
        firstName,
        lastName,
        startWorkAt,
        endWorkAt
      }
    });
    res.json(employee);
  } catch (error) {
    res.status(500).json({ error });

  }
}
export const remove = async (req, res) => {
  try {
    const { id } = req.params;
    const employee = await prisma.employee.delete({
      where: {
        id: Number(id)
      },
    });
    res.json(employee);
  } catch (error) {
    res.status(500).json({
      message: error.message
    })
  }
}
