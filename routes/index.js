import { PrismaClient } from '@prisma/client';
import express from 'express';
import multer from 'multer';
import * as Employee from '../controllers/Employee/EmployeeController.js';
import * as Entry from '../controllers/Entry/EntryController.js';
import * as Buy from '../controllers/Buy/BuyController.js';
import * as Cost from '../controllers/Cost/CostController.js';
import * as CostType from '../controllers/Cost/CostTypeController.js';
import * as Withdrawal from '../controllers/Withdrawal/withdrawalController.js';

const upload = multer();
var router = express.Router();


router.get('entry', (req, res) => { });
router.get('cost-type', (req, res) => { });

router.get('cost', async (req, res) => { });

router.get('/profile/:id', Employee.get);
router.get('/employee', Employee.getAll);
router.post('/employee', Employee.create);
router.delete('/employee/:id', Employee.remove);
router.put('/employee/:id', Employee.update);

router.get('/employee/:id/phone', Employee.PhoneNumber.get);
router.post('/employee/:id/phone', Employee.PhoneNumber.create);
router.delete('/phone/:phoneId', Employee.PhoneNumber.remove);
router.put('/employee/:id/phone/:phoneId', Employee.PhoneNumber.update);

router.post('/employee/:id/image', upload.single('picture'), Employee.Image.create);
router.put('/employee/:id/image', upload.single('picture'), Employee.Image.update);
router.delete('/employee/:id/image', Employee.Image.remove);

router.get('/employee/:id/email', Employee.Email.readAll);
router.post('/employee/:id/email', Employee.Email.create);
router.delete('/employee/:id/email/:emailId', Employee.Email.remove);
router.put('/employee/:id/email/:emailId', Employee.Email.update);

router.get('/employee/:id/address', Employee.Address.readAll);
router.post('/employee/:id/address', Employee.Address.create);
router.delete('/employee/:id/address/:addressId', Employee.Address.remove);
router.put('/employee/:id/address/:addressId', Employee.Address.update);

router.get('/entry', Entry.readAllPagination);
router.post('/entry', Entry.create);
router.delete('/entry/:id', Entry.remove);
router.put('/entry/:id', Entry.update);

router.get('/buy', Buy.readAllPagination);
router.post('/buy', Buy.create);
router.put('/buy/:id', Buy.update);
router.delete('/buy/:id', Buy.remove);



router.get('/cost', Cost.readAll);
router.post('/cost', Cost.create);
router.put('/cost/:id', Cost.update);
router.delete('/cost/:id', Cost.remove);

router.get('/cost-type', CostType.readAll);
router.post('/cost-type', CostType.create);
router.put('/cost-type/:id', CostType.update);
router.delete('/cost-type/:id', CostType.remove);

router.get('/withdrawal', Withdrawal.readAll);
router.post('/withdrawal', Withdrawal.create);
router.put('/withdrawal/:id', Withdrawal.update);
router.delete('/withdrawal/:id', Withdrawal.remove);

export default router;
