import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const get = async (req, res) => {
  try {
    const { id } = req.params;
    const phoneNumbers = await prisma.phoneNumber.findMany({
      where: {
        employeeId: Number(id)
      }
    });
    res.json(phoneNumbers);
  } catch (error) {
    res.status(500).json({
      message: error.message
    })
  }
}

export const create = async (req, res) => {
  try {
    const { id } = req.params;
    const { phoneNumber } = req.body;
    const phoneNumbers = await prisma.phoneNumber.create({
      data: {
        phoneNumber,
        employee: {
          connect: {
            id: Number(id)
          }
        }
      }
    });
    res.json(phoneNumbers);
  } catch (error) {
    res.status(500).json({ error });
  }
}

export const update = async (req, res) => {
  try {
    const { id, phoneId } = req.params;
    const { phoneNumber } = req.body;
    const numbers = await prisma.phoneNumber.update({
      where: {
        id: Number(phoneId)
      },
      data: {
        phoneNumber
      }
    });
    res.json(numbers);
  } catch (error) {
    res.status(500).json({ error });
  }
}

export const remove = async (req, res) => {
  try {
    const { id, phoneId } = req.params;
    const numbers = await prisma.phoneNumber.delete({
      where: {
        id: Number(phoneId)
      }
    });
    res.json(numbers);
  } catch (error) {
    res.status(500).json({ error });    
  }
}