
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const read = async (req, res) => {
  try {
    const { id } = req.params;
    const employeeEmail = await prisma.email.findOne({
      where: { id },
    });
    res.json(employeeEmail);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// read all
export const readAll = async (req, res) => {
  try {
    const {
      id
    } = req.params;
    const employeeEmails = await prisma.email.findMany({
      where: {
        employeeId: Number(id),
      }
    });
    res.json(employeeEmails);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// read All use pagination
export const readAllPagination = async (req, res) => {
  try {
    const { page, perPage } = req.query;
    const employeeEmails = await prisma.email.findMany({
      skip: perPage * (page - 1),
      take: perPage,
    });
    res.json(employeeEmails);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// create
export const create = async (req, res) => {
  try {
    const { id } = req.params;
    const { email } = req.body;
    const employeeEmail = await prisma.email.create({
      data: {
        email,
        employee: {
          connect: {
            id: Number(id),
          }
        },
      },
    });
    res.json(employeeEmail);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// update
export const update = async (req, res) => {
  try {
    const {
      id,
      emailId
    } = req.params;
    const { email } = req.body;
    const employeeEmail = await prisma.employee.update({
      where: {
        id: Number(id),
      },
      data: {
        emails: {
          update: [
            {
              where: {
                id: Number(emailId),
              },
              data: {
                email,
              },
            }
          ],
        },
      },
      include:{
        emails: true
      }
    });
    res.json(employeeEmail);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// remove 
export const remove = async (req, res) => {
  try {
    const { id, emailId } = req.params;
    const employee = await prisma.employee.update({
      where: {
        id: Number(id),
      },
      data: {
        emails: {
          deleteMany: [{ id: Number(emailId) }],
        },
      },
    })
    res.json(employee);
  } catch (error) {
    res.status(500).json({ error });
  }
}
