
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient({
  log: ['error', 'query']
});

const paginateGenerator = (count, perPage, page) => ({
  total: count,
  perPage: Number(perPage),
  page: Number(page),
  lastPage: Math.ceil(count / perPage)
});

const getCount = async () => {
  try {
    const count = await prisma.entry.count();
    return count;
  } catch (error) {
    throw error;
  }
}

export const read = async (req, res) => {
  try {

  } catch (error) {
    res.status(500).json({ error });
  }
}

// read all
export const readAll = async (req, res) => {
  try {
    const entries = await prisma.entry.findMany();
    res.json(entries);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// read All use pagination
export const readAllPagination = async (req, res) => {
  try {
    const {
      page = 1,
      perPage = 10
    } = req.query;

    const entries = await prisma.entry.findMany({
      skip: perPage * (page - 1),
      take: Number(perPage)
    });

    const count = getCount();

    const result = {
      entries,
      meta: paginateGenerator(count, perPage, page)
    }
    res.json(result);
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ error });
  }
}

// create
export const create = async (req, res) => {
  try {
    const {
      cardReader,
      cash,
      incomePayment,
      createdAt,
    } = req.body;

    const Entry = await prisma.entry.create({
      data: {
        cardReader: Number(cardReader),
        cash: Number(cash),
        incomePayment: Number(incomePayment),
        createdAt: new Date(createdAt)
      }
    });

    res.json(Entry);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// update
export const update = async (req, res) => {
  try {
    const {
      id
    } = req.params;
    const {
      cardReader,
      cash,
      incomePayment,
    } = req.body;

    const entry = await prisma.entry.update({
      where: {
        id: Number(id),
      },
      data: {
        cardReader: Number(cardReader),
        cash: Number(cash),
        incomePayment: Number(incomePayment)
      }
    });

    res.json(entry);

  } catch (error) {
    res.status(500).json({ error });
  }
}

// remove 
export const remove = async (req, res) => {
  try {
    const {
      id
    } = req.params;

    const entry = await prisma.entry.delete({
      where: {
        id: Number(id)
      }
    });

    res.json(entry)

  } catch (error) {
    res.status(500).json({ error });
  }
}
