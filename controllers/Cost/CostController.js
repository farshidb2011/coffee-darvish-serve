import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient({
  log: ['error', 'query']
});

export const read = async (req, res) => {
  try {

  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};

export const readAll = async (req, res) => {
  try {
    const cost = await prisma.cost.findMany({
      include:{
        type: true
      }
    });
    res.json({
      cost,
      meta: null
    });
  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};

export const readAllPagination = async (req, res) => {
  try {

  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};

export const create = async (req, res) => {
  try {
    const {
      type,
      value,
      createdAt
    } = req.body;

    const cost = await prisma.cost.create({
      data: {
        value: BigInt(value),
        createdAt: new Date(createdAt),
        type:{
          connect: {
            id: Number(type)
          }
        }
      },
      include:{
        type: true
      }
    });

    res.json({
      cost,
      meta: null
    });
  } catch (error) {
    res.status(500).json({
      msg: error.message
    });
  }
};

export const update = async (req, res) => {
  try {
    const { id } = req.params;
    const {
      createdAt,
      type,
      value
    } = req.body;

    const cost = await prisma.cost.update({
      where: {
        id: Number(id)
      },
      data: {
        typeId: type,
        value: BigInt(value),
        createdAt: new Date(createdAt)
      },
      include: {
        type: true
      }
    })

    res.json({
      cost,
      meta: null
    });

  } catch (error) {
    res.status(500).json({
      msg: error.message
    });
  }
};

export const remove = async (req, res) => {
  try {
    const { id } = req.params;
    const cost = await prisma.cost.delete({
      where: {
        id: Number(id)
      }
    })
    res.json({
      cost,
      meta: null
    }); 

  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};
