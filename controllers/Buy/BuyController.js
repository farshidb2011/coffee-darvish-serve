
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const read = async (req, res) => {
  try {
    const {id} = req.params;
    const buy = await prisma.buy.findUnique({
      where: {
        id: Number(id)
      }
    }); 
    res.json(buy);

  } catch (error) {
    res.status(500).json({
      message: error.message,
      code: error.code,
      ...error
    })
  }
}

export const readAllPagination = async (req, res) => {
  try {
    const {
      page = 1,
      perPage = 10,
    } = req.query;

    const purchases = await prisma.buy.findMany({
      take: Number(perPage),
      skip: perPage * (page - 1),
      orderBy: {
        id: 'desc'
      },
      include: {
        via: true,
      }
    });

    const count = await prisma.buy.count();

    const result = {
      purchases,
      meta: {
        perPage: Number(perPage),
        page: Number(page),
        total: count,
        lastPage: Math.ceil(count / perPage)
      }
    }

    res.json(result);

  } catch (error) {
    res.status(500).json({
      message: error.message,
      code: error.code,
      ...error
    })
  }
}

export async function create(req, res) {
  try {
    const {
      via,
      value,
      description
    } = req.body;

    const buy = await prisma.buy.create({
      data: {
        value: BigInt(value),
        description,
        via: {
          connect: {
            id: Number(via)
          }
        }
      },
      include: {
        via: true
      }
    });

    res.json(buy);
  } catch (error) {
    res.status(500).json({
      message: error.message,
      code: error.code,
      ...error
    })
  }
}

export const update = async (req, res) => {
  try {
    const {
      id
    } = req.params;
    const {
      via,
      value,
      description,
      createdAt
    } = req.body;

    const buy = await prisma.buy.update({
      where: {
        id: Number(id),
      },
      data: {
        via: {
          connect: {
            id: Number(via)
          }
        },
        value: BigInt(value),
        description,
        createdAt: new Date(createdAt)
      },
      include:{
        via: true
      }
    })

    res.json(buy)
  } catch (error) {
    res.status(500).json({
      message: error.message,
      code: error.code,
      ...error
    })
  }
}

export const remove = async (req, res) => {
  try {
    const {
      id
    } = req.params;
    const buy = await prisma.buy.delete({
      where: {
        id: Number(id)
      }
    });
    res.json(buy);
  } catch (error) {
    res.status(500).json({
      message: error.message,
      code: error.code,
      ...error
    })
  }
}

