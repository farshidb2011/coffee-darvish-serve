import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();


export const get = async (req, res) => {

}
export const create = async (req, res) => {
  const { id } = req.params;
  const Method = {
    create: {
      func: prisma.picture.create,
      args: {
        data: {
          pic: req.file.buffer,
          employee: {
            connect: {
              id: Number(id)
            }
          }
        }
      }
    },
    update: {
      func: prisma.picture.update,
      args: {
        where: {
          employeeId: Number(id)
        },
        data: {
          pic: req.file.buffer
        }
      }
    }
  }

  try {
    // check employee has image
    const hasImage = await prisma.picture.findUnique({
      where: {
        employeeId: Number(id)
      }
    });

    function getMethod() {
      if (hasImage) {
        // no has iamge return update function
        return Method.update;
      } else {
        // has image return create function
        return Method.create;
      }
    }
    const method = getMethod();
    await method.func(method.args);

    res.json({ id });
  } catch (error) {
    res.status(500).json({ error });
  }
}
export const update = async (req, res) => {
  try {
    const { id } = req.params;
    const employee = await prisma.employee.update({
      where: {
        id: Number(id)
      },
      data: {
        pic: req.file.buffer
      }
    });
    res.json(employee);
  } catch (error) {
    res.status(500).json({ error });
  }
}
export const remove = async (req, res) => {
  try {
    const { id } = req.params;
    const picture = await prisma.picture.delete({
      where: {
        employeeId: Number(id)
      }
    });
    res.json({ id });
  } catch (error) {
    res.status(500).json({ error });
  }
}