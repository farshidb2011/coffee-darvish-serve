
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const read = async (req, res) => {
  try {
    const { id } = req.params;
    const employeeAddress = await prisma.address.findOne({
      where: { id },
    });
    res.json(employeeAddress);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// read all
export const readAll = async (req, res) => {
  try {
    const {
      id
    } = req.params;
    const employeeAddress = await prisma.address.findMany({
      where: {
        employeeId: Number(id),
      }
    });
    res.json(employeeAddress);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// read All use pagination
export const readAllPagination = async (req, res) => {
  try {
    const { page, perPage } = req.query;
    const employeeAddress = await prisma.address.findMany({
      skip: perPage * (page - 1),
      take: perPage,
    });
    res.json(employeeAddress);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// create
export const create = async (req, res) => {
  try {
    const { id } = req.params;
    const { address } = req.body;
    const employeeaddress = await prisma.address.create({
      data: {
        address,
        employee: {
          connect: {
            id: Number(id),
          }
        },
      },
    });
    res.json(employeeaddress);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// update
export const update = async (req, res) => {
  try {
    const {
      id,
      addressId
    } = req.params;
    const { address } = req.body;
    console.log(id, addressId, address);
    const employeeaddress = await prisma.employee.update({
      where: {
        id: Number(id),
      },
      data: {
        addresses: {
          update: [
            {
              where: {
                id: Number(addressId),
              },
              data: {
                address,
              },
            }
          ],
        },
      },
      include:{
        addresses: true
      }
    });
    res.json(employeeaddress);
  } catch (error) {
    res.status(500).json({ error });
  }
}

// remove 
export const remove = async (req, res) => {
  try {
    const { id, addressId } = req.params;
    console.log(id, addressId);
    const employee = await prisma.employee.update({
      where: {
        id: Number(id),
      },
      data: {
        addresses: {
          deleteMany: [{ id: Number(addressId) }],
        },
      },
    })
    res.json(employee);
  } catch (error) {
    res.status(500).json({ error });
  }
}
