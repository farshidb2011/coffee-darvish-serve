import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient({
  log: ['error', 'query']
});

export const read = async (req, res) => {
  try {

  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};

export const readAll = async (req, res) => {
  try {
    const types = await prisma.type.findMany();
    res.json(types);
  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};

export const create = async (req, res) => {
  try {

    const {
      type
    } = req.body;

    const newType = await prisma.type.create({
      data: {
        type
      }
    });

    res.json(newType);

  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};

export const update = async (req, res) => {
  try {
    const {id} = req.params;
    const {
      type
    } = req.body;
    const updated = await prisma.type.update({
      where: {
        id: Number(id),
      },
      data:{
        type
      }
    })
    res.json(updated);
  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};

export const remove = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedType = await prisma.type.delete({
      where: { id: Number(id) },
    });
    res.json(deletedType);
  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};
