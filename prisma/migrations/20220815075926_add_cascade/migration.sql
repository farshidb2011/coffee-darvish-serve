-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Email" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "email" TEXT NOT NULL,
    "employeeId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    CONSTRAINT "Email_employeeId_fkey" FOREIGN KEY ("employeeId") REFERENCES "Employee" ("id") ON DELETE CASCADE ON UPDATE SET NULL
);
INSERT INTO "new_Email" ("createdAt", "email", "employeeId", "id", "updatedAt") SELECT "createdAt", "email", "employeeId", "id", "updatedAt" FROM "Email";
DROP TABLE "Email";
ALTER TABLE "new_Email" RENAME TO "Email";
CREATE TABLE "new_Address" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "address" TEXT NOT NULL,
    "employeeId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    CONSTRAINT "Address_employeeId_fkey" FOREIGN KEY ("employeeId") REFERENCES "Employee" ("id") ON DELETE CASCADE ON UPDATE SET NULL
);
INSERT INTO "new_Address" ("address", "createdAt", "employeeId", "id", "updatedAt") SELECT "address", "createdAt", "employeeId", "id", "updatedAt" FROM "Address";
DROP TABLE "Address";
ALTER TABLE "new_Address" RENAME TO "Address";
CREATE TABLE "new_PhoneNumber" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "phoneNumber" TEXT NOT NULL,
    "employeeId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    CONSTRAINT "PhoneNumber_employeeId_fkey" FOREIGN KEY ("employeeId") REFERENCES "Employee" ("id") ON DELETE CASCADE ON UPDATE SET NULL
);
INSERT INTO "new_PhoneNumber" ("createdAt", "employeeId", "id", "phoneNumber", "updatedAt") SELECT "createdAt", "employeeId", "id", "phoneNumber", "updatedAt" FROM "PhoneNumber";
DROP TABLE "PhoneNumber";
ALTER TABLE "new_PhoneNumber" RENAME TO "PhoneNumber";
CREATE TABLE "new_WithdrawalOnEmployee" (
    "employeeId" INTEGER NOT NULL,
    "withdrawalId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,

    PRIMARY KEY ("employeeId", "withdrawalId"),
    CONSTRAINT "WithdrawalOnEmployee_employeeId_fkey" FOREIGN KEY ("employeeId") REFERENCES "Employee" ("id") ON DELETE CASCADE ON UPDATE SET NULL,
    CONSTRAINT "WithdrawalOnEmployee_withdrawalId_fkey" FOREIGN KEY ("withdrawalId") REFERENCES "Withdrawal" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_WithdrawalOnEmployee" ("createdAt", "employeeId", "updatedAt", "withdrawalId") SELECT "createdAt", "employeeId", "updatedAt", "withdrawalId" FROM "WithdrawalOnEmployee";
DROP TABLE "WithdrawalOnEmployee";
ALTER TABLE "new_WithdrawalOnEmployee" RENAME TO "WithdrawalOnEmployee";
CREATE TABLE "new_Buy" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "employeeId" INTEGER NOT NULL,
    "value" BIGINT NOT NULL,
    "description" TEXT,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    CONSTRAINT "Buy_employeeId_fkey" FOREIGN KEY ("employeeId") REFERENCES "Employee" ("id") ON DELETE CASCADE ON UPDATE SET NULL
);
INSERT INTO "new_Buy" ("createdAt", "description", "employeeId", "id", "updatedAt", "value") SELECT "createdAt", "description", "employeeId", "id", "updatedAt", "value" FROM "Buy";
DROP TABLE "Buy";
ALTER TABLE "new_Buy" RENAME TO "Buy";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
