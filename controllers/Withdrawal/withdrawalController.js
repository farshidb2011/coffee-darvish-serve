import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient({
  log: ['error', 'query']
});

export const read = async (req, res) => {
  try {

  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};

export const readAll = async (req, res) => {
  try {
    const withdrawal = await prisma.withdrawal.findMany({
      include:{
        Employee: true
      }
    });
    res.json({
      withdrawal,
      meta: null
    });
  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};

export const readAllPagination = async (req, res) => {
  try {

  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};

export const create = async (req, res) => {
  try {
    const {
      employee,
      value,
      createdAt
    } = req.body;

    const withdrawal = await prisma.withdrawal.create({
      data: {
        value: BigInt(value),
        createdAt: new Date(createdAt),
        Employee:{
          connect: {
            id: Number(employee)
          }
        }
      },
      include:{
        Employee: true
      }
    });

    res.json({
      withdrawal,
      meta: null
    });
  } catch (error) {
    res.status(500).json({
      msg: error.message
    });
  }
};

export const update = async (req, res) => {
  try {
    const { id } = req.params;
    const {
      createdAt,
      employee,
      value
    } = req.body;

    const withdrawal = await prisma.withdrawal.update({
      where: {
        id: Number(id)
      },
      data: {
        employeeId: Number(employee),
        value: BigInt(value),
        createdAt: new Date(createdAt)
      },
      include: {
        Employee: true
      }
    })

    res.json({
      withdrawal,
      meta: null
    });

  } catch (error) {
    res.status(500).json({
      msg: error.message
    });
  }
};

export const remove = async (req, res) => {
  try {
    const { id } = req.params;
    const withdrawal = await prisma.withdrawal.delete({
      where: {
        id: Number(id)
      }
    })
    res.json({
      withdrawal,
      meta: null
    }); 

  } catch (error) {
    res.status(500).json({
      ...error
    });
  }
};
